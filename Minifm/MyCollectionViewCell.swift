//
//  MyCollectionViewCell.swift
//  Minifm
//
//  Created by Aradhana Ray on 06/02/18.
//  Copyright © 2018 Aradhana Ray. All rights reserved.
//

import UIKit

class MyCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblName: UILabel!
}
