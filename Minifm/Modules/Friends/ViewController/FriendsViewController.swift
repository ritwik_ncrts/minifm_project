//
//  FriendsViewController.swift
//  Minifm
//
//  Created by Aradhana Ray on 08/02/18.
//  Copyright © 2018 Aradhana Ray. All rights reserved.
//

import UIKit

class FriendsViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblMain: UITableView!
    @IBOutlet weak var vwSearch: UISearchBar!
    
    var arrFriends = ["1","2","3","4","5"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblMain.tableFooterView = UIView(frame: CGRect.zero)
        let nibName = UINib(nibName: "FriendsTableViewCell", bundle:nil)
        tblMain.register(nibName, forCellReuseIdentifier: "FriendsTableViewCell")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func tapMenuAction(_ sender: Any) {
        slideMenuController()?.toggleLeft()
    }
    
    @IBAction func btnInviteFriends(_ sender: Any) {
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.contentView.backgroundColor = UIColor.black
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrFriends.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 67.0;     //Choose your custom row height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendsTableViewCell", for: indexPath) as! FriendsTableViewCell
       /* let Dict = self.arrNotifications[indexPath.row]
        cell.tvNotification.text = Dict.msg
        cell.lblWeight.text = Dict.weight + " KG"
        cell.lblTrackId.text = Dict.TrackId
        
        if (cell.lblTrackId.text) == "0"{
            
            cell.lblTrackId.isHidden = true
        }else{
            cell.lblTrackId.isHidden = false
        }
        let strDate = changeDateString(Dict.date)
        cell.lblDate.text = strDate*/
        cell.selectionStyle = .none
        return cell
        
    }
    
    
}
