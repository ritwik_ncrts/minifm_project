//
//  SellViewController.swift
//  Minifm
//
//  Created by Aradhana Ray on 07/02/18.
//  Copyright © 2018 Aradhana Ray. All rights reserved.
//

import UIKit

class SellViewController: UIViewController {

    @IBOutlet weak var tfMakePrice: UITextField!
    @IBOutlet weak var tfPrice: UITextField!
    @IBOutlet weak var tfShipping: UITextField!
    @IBOutlet weak var tvDesc: UITextView!
    @IBOutlet weak var tfCondition: UITextField!
    @IBOutlet weak var tfBrand: UITextField!
    @IBOutlet weak var tfSize: UITextField!
    @IBOutlet weak var tfCategory: UITextField!
    @IBOutlet weak var tfGen: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tvDesc.text = "Descripition here"
        tvDesc.textColor = UIColor.lightGray
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Descripition here"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        return true
    }
    
    
    @IBAction func addPhotoAction(_ sender: Any) {
    }
    
    @IBAction func createListAction(_ sender: Any) {
        
    }
    
}
