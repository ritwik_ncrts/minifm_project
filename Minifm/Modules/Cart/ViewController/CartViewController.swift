//
//  CartViewController.swift
//  Minifm
//
//  Created by Aradhana Ray on 08/02/18.
//  Copyright © 2018 Aradhana Ray. All rights reserved.
//

import UIKit

class CartViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblMain: UITableView!

    var arrCart = ["1","2","3","4","5"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblMain.tableFooterView = UIView(frame: CGRect.zero)
        let nibName = UINib(nibName: "CartTableViewCell", bundle:nil)
        tblMain.register(nibName, forCellReuseIdentifier: "CartTableViewCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func tapMenuAction(_ sender: Any) {
        
        slideMenuController()?.toggleLeft()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //cell.contentView.backgroundColor = UIColor.black
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrCart.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 315.0;     //Choose your custom row height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartTableViewCell", for: indexPath) as! CartTableViewCell
        
        
        cell.selectionStyle = .none
        return cell
        
    }


}
