//
//  CartTableViewCell.swift
//  Minifm
//
//  Created by Aradhana Ray on 08/02/18.
//  Copyright © 2018 Aradhana Ray. All rights reserved.
//

import UIKit

class CartTableViewCell: UITableViewCell {

    @IBOutlet weak var btnCheckOut: UIButton!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblShipping: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var tvDesc: UITextView!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
