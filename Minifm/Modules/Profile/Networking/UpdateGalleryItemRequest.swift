//
//  UpdateGalleryItemRequest.swift
//  Minifm
//
//  Created by Sayanti on 4/12/18.
//  Copyright © 2018 Aradhana Ray. All rights reserved.
//

import Foundation
import Alamofire


class UpdateGalleryItemRequest: APIRequest
{
    public var path: String {
        return "vieworderhistory"
    }
    public var header: [String : String]{
        
        return ["Content-Type": "application/json" , "JWTTOKEN":userId]
    }
    public var method: HTTPMethod {
        return .post
    }
    
    public var parameters: [String: Any] {
        return ["order_id": order_id]
    }
    
    public var isAuthorized: Bool {
        return false
    }
    
    public var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    private let userId: String
    private let order_id: String
    
    init(user_id: String,order_id: String) {
        
        self.userId = user_id
        self.order_id = order_id
        
    }
}
