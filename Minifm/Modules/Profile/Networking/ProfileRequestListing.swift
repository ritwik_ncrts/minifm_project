//
//  ProfileRequest.swift
//  Minifm
//
//  Created by Sayanti on 4/10/18.
//  Copyright © 2018 Aradhana Ray. All rights reserved.
//

import Foundation
import Alamofire


class ProfileRequest1_1: APIRequest
{
    public var path: String {
        return "myproductlisting"
    }
    public var header: [String : String]{
        
        return ["Content-Type": "application/json" , "JWTTOKEN":userId]
    }
    public var method: HTTPMethod {
        return .get
    }
    
    public var parameters: [String: Any] {
        return [:]
    }
    
    public var isAuthorized: Bool {
        return false
    }
    
    public var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    private let userId: String
    
    
    init(user_id: String) {
        
        self.userId = user_id
        
    }
}
