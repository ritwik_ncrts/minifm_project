//
//  ProfileResponse1_1.swift
//  Minifm
//
//  Created by Sayanti on 4/10/18.
//  Copyright © 2018 Aradhana Ray. All rights reserved.
//

import Foundation
import ObjectMapper

class ProfileResponseListing: Mappable
    
{
    var arrList = [ProfileModelListing]()
    
    public required init?(map: Map)
    {
        
    }
    
    public func mapping(map: Map)
    {
        arrList <- map["data"]
        print(arrList.count)
    }
}
