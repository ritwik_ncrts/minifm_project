//
//  UpdateGalleryItemModel.swift
//  Minifm
//
//  Created by Sayanti on 4/12/18.
//  Copyright © 2018 Aradhana Ray. All rights reserved.
//

import Foundation
import ObjectMapper

class UpdateGalleryItemModel: Mappable
    
{
    
    var product_id: String!
    var image: String!
    var title: String!
    var shipping: String!
    var list_price: String!
    
    public required init?(map: Map)
    {
        
    }
    
    public func mapping(map: Map)
    {
        
        product_id <- map["product_id"]
        title <- map["title"]
        shipping <- map["shipping"]
        image <- map["image"]
        list_price <- map["list_price"]
        
        
        // print("login user_id",user_id)
    }
}
