//
//  SettingsViewController.swift
//  Minifm
//
//  Created by Aradhana Ray on 09/02/18.
//  Copyright © 2018 Aradhana Ray. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblMain: UITableView!
    var rootViewController:UIViewController!
    
    var arrSettings = ["Shop Settings","Notifications","Gallery","Address","Payment Settings","Change Password","Logout"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblMain.tableFooterView = UIView(frame: CGRect.zero)
        let nibName = UINib(nibName: "SettingsTableViewCell", bundle:nil)
        tblMain.register(nibName, forCellReuseIdentifier: "SettingsTableViewCell")
        
        //  For logout
         let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let rootController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.rootViewController = UINavigationController(rootViewController: rootController)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
  
    
    @IBAction func tapMenuAction(_ sender: Any) {
        
        slideMenuController()?.toggleLeft()
        
    }
    @IBAction func alertAction(_ sender: Any) {
    }
    @IBAction func messageAction(_ sender: Any) {
    }
    @IBAction func allReadAction(_ sender: Any) {
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.contentView.backgroundColor = UIColor.black
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrSettings.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 55.0;     //Choose your custom row height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTableViewCell", for: indexPath) as! SettingsTableViewCell
        
        cell.lblTitle.text = self.arrSettings[indexPath.row]
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        
        if indexPath.row == 0 {
            
            let moveViewController = ShopSettingsForCustomerViewController()
            self.navigationController?.pushViewController(moveViewController, animated: true)
            
        }else if indexPath.row == 1 {
            
            let moveViewController = AddGalleryViewController()
            self.navigationController?.pushViewController(moveViewController, animated: true)
            
        }else if indexPath.row == 2 {
            
            let moveViewController = AddGalleryViewController()
            self.navigationController?.pushViewController(moveViewController, animated: true)
            
        }else if indexPath.row == 3 {
            
            let moveViewController = AddressViewController()
            self.navigationController?.pushViewController(moveViewController, animated: true)
            
        }else if indexPath.row == 4 {
            
            let moveViewController = AddGalleryViewController()
            self.navigationController?.pushViewController(moveViewController, animated: true)
            
            
        }else if indexPath.row == 5 {
            
            let moveViewController = ChangePasswordViewController()
            self.navigationController?.pushViewController(moveViewController, animated: true)
            
            
        }else if indexPath.row == 6 {
            
            let defaults = UserDefaults.standard
            defaults.removeObject(forKey:"user_id")
            defaults.synchronize()
            self.slideMenuController()?.changeMainViewController(self.rootViewController, close: true)
            
        }
       
        
    }

}
