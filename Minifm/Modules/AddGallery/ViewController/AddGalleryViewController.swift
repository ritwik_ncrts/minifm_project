//
//  AddGalleryViewController.swift
//  Minifm
//
//  Created by Aradhana Ray on 10/02/18.
//  Copyright © 2018 Aradhana Ray. All rights reserved.
//

import UIKit

class AddGalleryViewController: UIViewController {

    @IBOutlet weak var imgProduct:UIImageView!
     var correctedImage:UIImage!
    override func viewDidLoad() {
        super.viewDidLoad()
        ChoosePic ()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        return true
    }
    
    func ChoosePic () {
        
        let optionMenu = UIAlertController(title: nil, message: "Choose Photo", preferredStyle: .actionSheet)
        
        //Choose Option
        
        // 2
        let cameraAction = UIAlertAction(title: "Open Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .camera
            
            self.present(imagePicker, animated: true, completion: nil)
            
        })   //From Camera
        let libraryAction = UIAlertAction(title: "Open Gallery", style: .default, handler: {
            
            (alert: UIAlertAction!) -> Void in
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .photoLibrary
            
            imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            self.present(imagePicker, animated: true, completion: nil)
            
        })   //From Gallery
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
        })
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(libraryAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
        
        
    }

    @IBAction func createAction(_ sender: Any) {
        
        
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
}

extension AddGalleryViewController:UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]){
        
        correctedImage = info[UIImagePickerControllerOriginalImage] as! UIImage!
        imgProduct.image = correctedImage // correctedImage.fixOrientation()
      //  updateProfilePicApiCall()
        
        self.dismiss(animated: true, completion: nil)
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        
        dismiss(animated: true, completion: nil)
    }
    
    
}
