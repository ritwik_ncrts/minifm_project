//
//  GallerySettingsRequest.swift
//  Minifm
//
//  Created by Dipanjan on 3/26/18.
//  Copyright © 2018 Aradhana Ray. All rights reserved.
//

import Foundation
import Alamofire


class GallerySettingsRequest: APIRequest
{
    public var path: String {
        return "gallerysettings"
    }
    public var header: [String : String]{
        
        return ["Content-Type": "application/json", "JWTTOKEN" : user_id]
    }
    public var method: HTTPMethod {
        return .post
    }
    
    public var parameters: [String: Any] {
        return ["gallery_flag": gallery_flag, "user_id" : user_id]
    }
    
    public var isAuthorized: Bool {
        return false
    }
    
    public var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    private let gallery_flag: String
    private let user_id: String
    
    init(gallery_flag: String, user_id: String) {
        
        self.gallery_flag = gallery_flag
        self.user_id = user_id
        
    }
}
