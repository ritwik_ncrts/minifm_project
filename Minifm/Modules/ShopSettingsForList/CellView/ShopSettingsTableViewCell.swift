//
//  ShopSettingsTableViewCell.swift
//  Minifm
//
//  Created by Aradhana Ray on 10/02/18.
//  Copyright © 2018 Aradhana Ray. All rights reserved.
//

import UIKit

class ShopSettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var imgDiscount: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
