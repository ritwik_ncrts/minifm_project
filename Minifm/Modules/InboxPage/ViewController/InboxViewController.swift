//
//  InboxViewController.swift
//  Minifm
//
//  Created by Aradhana Ray on 08/02/18.
//  Copyright © 2018 Aradhana Ray. All rights reserved.
//

import UIKit

class InboxViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tblMain: UITableView!
    
    var arrInbox = ["1","2","3","4","5"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblMain.tableFooterView = UIView(frame: CGRect.zero)
        let nibName = UINib(nibName: "InboxTableViewCell", bundle:nil)
        tblMain.register(nibName, forCellReuseIdentifier: "InboxTableViewCell")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    
    @IBAction func tapMenuAction(_ sender: Any) {
        
        slideMenuController()?.toggleLeft()
        
    }
    @IBAction func alertAction(_ sender: Any) {
    }
    @IBAction func messageAction(_ sender: Any) {
    }
    @IBAction func allReadAction(_ sender: Any) {
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.contentView.backgroundColor = UIColor.black
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrInbox.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 60.0;     //Choose your custom row height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "InboxTableViewCell", for: indexPath) as! InboxTableViewCell
        
        cell.selectionStyle = .none
        return cell
        
    }
}
