//
//  HomeViewController.swift
//  Minifm
//
//  Created by Aradhana Ray on 05/02/18.
//  Copyright © 2018 Aradhana Ray. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        self.setHomeNavigationBarItem()
      /*  let leftButton: UIBarButtonItem = UIBarButtonItem(title: "Threads", style: .plain, target: self, action: #selector(toggleLeft))
        navigationItem.leftBarButtonItem = leftButton*/
        
       self.tabBarController?.navigationItem.hidesBackButton = true
        
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "btn-search"), for: .normal)
        
        
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(searchAction), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        //
        let btn5 = UIButton(type: .custom)
        btn5.setTitle("Threads", for:.normal)
        btn5.setTitleColor(UIColor.gray, for: .normal)
        btn5.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn5.addTarget(self, action: #selector(listingAction), for: .touchUpInside)
        let item5 = UIBarButtonItem(customView: btn5)
        
        
       // self.navigationItem.setRightBarButtonItems([item1,item5], animated: true)
        
        let btn3 = UIButton(type: .custom)
        btn3.setTitle("Featured", for:.normal)
        btn3.setTitleColor(UIColor.gray, for: .normal)
        btn3.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn3.addTarget(self, action: #selector(listingAction), for: .touchUpInside)
        let item3 = UIBarButtonItem(customView: btn3)
        
        let btn4 = UIButton(type: .custom)
        btn4.setImage(UIImage(named: "top-butt-menu"), for: .normal)
        btn4.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn4.addTarget(self, action: #selector(featuredAction), for: .touchUpInside)
        let item4 = UIBarButtonItem(customView: btn4)
        
        let btn2 = UIButton(type: .custom)
        btn2.setTitle("Listing", for:.normal)
        btn2.setTitleColor(UIColor.gray, for: .normal)
        btn2.frame = CGRect(x: 0, y: 0, width:60, height: 30)
        btn2.addTarget(self, action: #selector(listingAction), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn2)
        
        self.navigationItem.setLeftBarButtonItems([item4,item3,item2,item5,item1], animated: true)
        
    }
    
    func featuredAction() {
        
        
    }
    func listingAction() {
        
        
    }
    func threadsAction() {
        
        
    }
    func searchAction() {
        
        
    }
    public override func toggleLeft() {
        
        slideMenuController()?.toggleLeft()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
