//
//  ChangePasswordViewController.swift
//  Minifm
//
//  Created by Aradhana Ray on 09/02/18.
//  Copyright © 2018 Aradhana Ray. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {

    @IBOutlet weak var tfCurrentPass: UITextField!
    
    @IBOutlet weak var tfNewPassword: UITextField!
    
    
    @IBOutlet weak var tfConfirmPass: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        return true
    }
  
    @IBAction func btnSaveAction(_ sender: Any) {
        
        
    }
    
    
}
