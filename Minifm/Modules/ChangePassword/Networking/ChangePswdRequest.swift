//
//  ChangePswdRequest.swift
//  Minifm
//
//  Created by Dipanjan on 3/19/18.
//  Copyright © 2018 Aradhana Ray. All rights reserved.
//

import Foundation
import Alamofire


class ChangePswdRequest: APIRequest
{
    public var path: String {
        return "changepassword"
    }
    
    public var method: HTTPMethod {
        return .post
    }
    
    public var parameters: [String: Any] {
        return ["username": email, "password": password, "device_token": deviceToken]
    }
    
    public var isAuthorized: Bool {
        return false
    }
    
    public var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    private let email: String
    private let password: String
    private let deviceToken: String
    
    init(username: String, password: String,device_token: String) {
        
        self.email = username
        self.password = password
        self.deviceToken = device_token
        
    }
}
