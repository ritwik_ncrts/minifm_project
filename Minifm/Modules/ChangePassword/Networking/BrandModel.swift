//
//  BrandModel.swift
//  Minifm
//
//  Created by Dipanjan on 3/19/18.
//  Copyright © 2018 Aradhana Ray. All rights reserved.
//

import Foundation
class BrandModel: Mappable
    
{
    var user_id: String!
    var email: String!
    var location: String!
    var phone: String!
    var profile_image: String!
    var fname: String!
    var lname: String!
    var company_name: String!
    
    public required init?(map: Map)
    {
        
    }
    
    public func mapping(map: Map)
    {
        user_id <- map["token"]
        email <- map["data.email"]
        fname <- map["data.firstname"]
        lname <- map["data.lastname"]
        
        //phone <- map["Details.phone"]
        //  profile_image <- map["UserDetails.profile_image"]
        print("login user_id",user_id)
}
