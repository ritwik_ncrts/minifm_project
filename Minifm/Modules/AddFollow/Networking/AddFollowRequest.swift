//
//  FollowRequest.swift
//  Minifm
//
//  Created by Sayanti on 3/31/18.
//  Copyright © 2018 Aradhana Ray. All rights reserved.
//

import Foundation
import Alamofire


class AddFollowRequest: APIRequest
{
    public var path: String {
        return "followfriend"
    }
    public var header: [String : String]{
        
        return ["Content-Type": "application/json" , "JWTTOKEN":userId]
    }
    public var method: HTTPMethod {
        return .post
    }
    
    public var parameters: [String: Any] {
        return ["userId":userId,"following_id":following_id,"status":status]
    }
    
    public var isAuthorized: Bool {
        return false
    }
    
    public var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    private let userId: String
    private let following_id: String
     private let status: String
    init(userId:String,following_id: String,status: String) {
        
        self.userId = userId
        self.following_id = following_id
        self.status = status
        
    }
}
