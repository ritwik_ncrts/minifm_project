//
//  AddFollowModel.swift
//  Minifm
//
//  Created by Sayanti on 3/31/18.
//  Copyright © 2018 Aradhana Ray. All rights reserved.
//

import Foundation
import ObjectMapper

class AddFollowModel: Mappable
    
{
    var message : String!
    
    
    
    public required init?(map: Map)
    {
        
    }
    
    public func mapping(map: Map)
    {
        message <- map["message"]
        
        
    }
    
}
