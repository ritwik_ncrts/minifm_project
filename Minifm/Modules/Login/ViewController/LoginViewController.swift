//
//  LoginViewController.swift
//  Minifm
//
//  Created by Aradhana Ray on 05/02/18.
//  Copyright © 2018 Aradhana Ray. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

     var deviceToken:String!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func loginSubmitAction(_ sender: Any) {
        
        if InternetRechability.isConnectedToNetwork() == true {
            
            //let moveViewController = AddAddressViewController()
            //self.navigationController?.pushViewController(moveViewController, animated: true)
            
            print("Internet connection OK")
            let prefs = UserDefaults.standard
            deviceToken = prefs.string(forKey:"DeviceToken")
            print(deviceToken)
            if tfEmail.text == ""
            {
                Alert.disPlayAlertMessage(titleMessage: "Sorry!", alertMsg: "Please enter your email.")
            }else if tfPassword.text == ""
            {
                Alert.disPlayAlertMessage(titleMessage: "Sorry!", alertMsg: "Please enter password.")
            }
            
        }
    }
    
    @IBAction func fbLoginAction(_ sender: Any) {
    }
    
    
    @IBAction func forgotPassword(_ sender: Any) {
        
        let moveViewController = ForgotPasswordViewController()
        self.navigationController?.pushViewController(moveViewController, animated: true)
    }
    @IBAction func signUpAction(_ sender: Any) {
        
        let moveViewController = SignUpViewController()
        self.navigationController?.pushViewController(moveViewController, animated: true)
    }
    func createView()  {
        
        let screenSize: CGRect = UIScreen.main.bounds
        let myView = UIView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height:screenSize.height))
        myView.backgroundColor = UIColor.black
        myView.alpha = 0.30
        self.view.addSubview(myView)
    }
}
