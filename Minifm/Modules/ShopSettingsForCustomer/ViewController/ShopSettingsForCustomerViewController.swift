//
//  ShopSettingsForCustomerViewController.swift
//  Minifm
//
//  Created by Aradhana Ray on 10/02/18.
//  Copyright © 2018 Aradhana Ray. All rights reserved.
//

import UIKit

class ShopSettingsForCustomerViewController: UIViewController {

    @IBOutlet weak var vwPicker: UIPickerView!
    var arrDiscount =  ["5%","10%","15%","20%","25%","30%","35%","40%","45%","50%","55%","60%","65%","70%","75%","80%","85%","90%"]
    var arrItems = ["1","2","3","4","5","6","7","8","9","10",]
    
    var recievedString: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("ShopSettingsForCustomerViewController")
        // Do any additional setup after loading the view.
        vwPicker.reloadAllComponents()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
        
    }
    
    
    /*func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
     {
        //row = [repeatPickerView selectedRowInComponent:0];
        var row = pickerView.selectedRow(inComponent: 0)
        print("this is the pickerView\(row)")
        
        if component == 0 {
            return self.arrDiscount.count
        }
            
        else {
            return arrItems.count
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if component == 0 {
            return String(arrDiscount[row])
        } else {
            
            return String(arrItems[row])
        }
    }*/

    
}

extension ShopSettingsForCustomerViewController :UIPickerViewDelegate,UIPickerViewDataSource {
    
  
   public func numberOfComponents(in pickerView: UIPickerView) -> Int {
    
        return 2
    }

   public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    
    
         var row = pickerView.selectedRow(inComponent: 0)
         print("this is the pickerView\(row)")
    
        if component == 0 {
             return self.arrDiscount.count
         }
        else {
           return arrItems.count
       }
    
    }
    
  public  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    
    if component == 0 {
        return String("\(arrDiscount[row]) off on")
    } else {
        
        return String("\(arrItems[row]) or more listings")
    }
    }
    
   public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
    
}
