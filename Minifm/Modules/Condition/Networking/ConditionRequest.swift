//
//  ConditionRequest.swift
//  Minifm
//
//  Created by Dipanjan on 3/23/18.
//  Copyright © 2018 Aradhana Ray. All rights reserved.
//

import Foundation
import Alamofire

class ConditionRequest: APIRequest
{
    public var path: String {
        return "getattributelist"
    }
    public var header: [String : String]{
        
        return ["Content-Type": "application/json"]
    }
    public var method: HTTPMethod {
        return .post
    }
    // email, username, password, device_token
    public var parameters: [String: Any] {
        return ["attribute": size]
    }
    
    public var isAuthorized: Bool {
        return false
    }
    
    public var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    private let size:String
    
    
    init(size: String) {
        
        self.size = size
        
    }
}
