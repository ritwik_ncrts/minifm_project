//
//  GalleryViewController.swift
//  Minifm
//
//  Created by Aradhana Ray on 09/02/18.
//  Copyright © 2018 Aradhana Ray. All rights reserved.
//

import UIKit

class GalleryViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblMain: UITableView!
    var arrGallery = ["1","2","3","4","5"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblMain.tableFooterView = UIView(frame: CGRect.zero)
        let nibName = UINib(nibName: "GalleryTableViewCell", bundle:nil)
        tblMain.register(nibName, forCellReuseIdentifier: "GalleryTableViewCell")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
    }

    @IBAction func tapMenuAction(_ sender: Any) {
        slideMenuController()?.toggleLeft()
    }
    @IBAction func addGalleryAction(_ sender:Any){
        
        let moveViewController = AddGalleryViewController()
        self.navigationController?.pushViewController(moveViewController, animated: true)
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //cell.contentView.backgroundColor = UIColor.black
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrGallery.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 452.0;     //Choose your custom row height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "GalleryTableViewCell", for: indexPath) as! GalleryTableViewCell
       
        
        cell.selectionStyle = .none
        return cell
        
    }

}
