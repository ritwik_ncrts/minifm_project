//
//  ListPageViewController.swift
//  Minifm
//
//  Created by Aradhana Ray on 07/02/18.
//  Copyright © 2018 Aradhana Ray. All rights reserved.
//

import UIKit

class ListPageViewController: UIViewController,UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet weak var vwCollection: UICollectionView!
    var photos = Photo.allPhotos()
    override func viewDidLoad() {
        super.viewDidLoad()

        print("check array count--",photos.count)
        
        
        self.vwCollection.register(AnnotatedCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnTap(_ sender: Any) {
         slideMenuController()?.toggleLeft()
        
    }
    override func toggleLeft() {
        slideMenuController()?.toggleLeft()
    }
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        if let annotateCell = cell as? AnnotatedCollectionViewCell {
            annotateCell.photo = photos[indexPath.item]
        }
        return cell
    }

}
extension ListPageViewController {
    
    
    
}

//MARK: - PINTEREST LAYOUT DELEGATE
extension ListPageViewController : ListCellLayoutDelegate {
    
    // 1. Returns the photo height
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath:IndexPath) -> CGFloat {
        return photos[indexPath.item].image.size.height
    }
    
}
