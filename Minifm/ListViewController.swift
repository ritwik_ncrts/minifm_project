//
//  ListViewController.swift
//  Minifm
//
//  Created by Aradhana Ray on 07/02/18.
//  Copyright © 2018 Aradhana Ray. All rights reserved.
//

import UIKit

class ListViewController: UIViewController,UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var vwListing: UIView!
    @IBOutlet weak var vwFeatured: UIView!
    @IBOutlet weak var vwThreads: UIView!
    @IBOutlet weak var btnThreads: UIButton!
    @IBOutlet weak var btnListing: UIButton!
    @IBOutlet weak var btnFeatured: UIButton!
    @IBOutlet weak var vwCollection: UICollectionView!
    var photos = Photo.allPhotos()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("check array count--",photos.count)
        
        btnListing.setTitleColor(UIColor.red, for: .normal)
        vwCollection?.backgroundColor = UIColor.clear
        vwCollection?.contentInset = UIEdgeInsets(top: 8, left: 6, bottom: 10, right: 6)
        
        if let layout = vwCollection?.collectionViewLayout as? PinterestLayout {
            layout.delegate = self
        }
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
        vwListing.isHidden = false
        vwThreads.isHidden = true
        vwFeatured.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnTap(_ sender: Any) {
        slideMenuController()?.toggleLeft()
        
    }
   
   
    
    
    @IBAction func featuredAction(_ sender: Any) {
    
        btnFeatured.setTitleColor(UIColor.red, for: .normal)
        btnListing.setTitleColor(UIColor.lightGray, for: .normal)
        btnThreads.setTitleColor(UIColor.lightGray, for: .normal)
        
        vwListing.isHidden = true
        vwThreads.isHidden = true
        vwFeatured.isHidden = false
    }
    
    @IBAction func listingAction(_ sender: Any) {
    
        btnListing.setTitleColor(UIColor.red, for: .normal)
        btnFeatured.setTitleColor(UIColor.lightGray, for: .normal)
        btnThreads.setTitleColor(UIColor.lightGray, for: .normal)
        
        vwListing.isHidden = false
        vwThreads.isHidden = true
        vwFeatured.isHidden = true
    }
    
     @IBAction func threadsAction(_ sender: Any) {
        
        btnThreads.setTitleColor(UIColor.red, for: .normal)
        btnListing.setTitleColor(UIColor.lightGray, for: .normal)
        btnFeatured.setTitleColor(UIColor.lightGray, for: .normal)
        
        vwListing.isHidden = true
        vwThreads.isHidden = false
        vwFeatured.isHidden = true
    }
    func searchAction() {
        
        
    }
    func slideMenuTap()  {
        
        slideMenuController()?.toggleLeft()
    }
    
}
extension ListViewController {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        if let annotateCell = cell as? AnnotatedCollectionViewCell {
            annotateCell.photo = photos[indexPath.item]
        }
        return cell
    }
     func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        print("check row --",indexPath.row)
    }
}

//MARK: - PINTEREST LAYOUT DELEGATE
extension ListViewController : ListCellLayoutDelegate {
    
    // 1. Returns the photo height
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath:IndexPath) -> CGFloat {
        return photos[indexPath.item].image.size.height
    }
    
}

