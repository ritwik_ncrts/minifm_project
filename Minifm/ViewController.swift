//
//  ViewController.swift
//  Minifm
//
//  Created by Aradhana Ray on 05/02/18.
//  Copyright © 2018 Aradhana Ray. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class ViewController: UIViewController {
    var user_id:String!
    var dict : [String : AnyObject]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let prefs = UserDefaults.standard
        user_id = prefs.string(forKey: "user_id")
        print(user_id)
        if (user_id==nil){
            
        }else{
            let moveViewController = HomeViewController()
            self.navigationController?.pushViewController(moveViewController, animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
    }

    @IBAction func signUpAction(_ sender: Any) {
        
        
        let secondviewController:UIViewController =  (self.storyboard?.instantiateViewController(withIdentifier: "ListViewController") as? ListViewController)!
        
        self.navigationController?.pushViewController(secondviewController, animated: true)
        
       // let moveViewController = SignUpViewController()
       // let moveViewController = ListPageViewController()
      //  let moveViewController = ListViewController()
      //  self.navigationController?.pushViewController(moveViewController, animated: true)
    }
    
    @IBAction func fbSignUpAction(_ sender: Any) {
      
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData()
                        fbLoginManager.logOut()
                    }
                }
            }
        }
    }
    
    
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    self.dict = result as! [String : AnyObject]
                    print(result!)
                    print(self.dict)
                }
            })
        }
    }

    @IBAction func loginAction(_ sender: Any) {
        
        let moveViewController = SellViewController()
        self.navigationController?.pushViewController(moveViewController, animated: true)
    }
}

