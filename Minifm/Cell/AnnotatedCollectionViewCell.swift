//
//  AnnotatedCollectionViewCell.swift
//  Minifm
//
//  Created by Aradhana Ray on 07/02/18.
//  Copyright © 2018 Aradhana Ray. All rights reserved.
//

import UIKit

class AnnotatedCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet fileprivate weak var containerView: UIView!
    @IBOutlet fileprivate weak var imageView: UIImageView!
    @IBOutlet fileprivate weak var captionLabel: UILabel!
    @IBOutlet fileprivate weak var commentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.cornerRadius = 6
        containerView.layer.masksToBounds = true
    }
    
    var photo: Photo? {
        didSet {
            if let photo = photo {
                imageView.image = photo.image
               // captionLabel.text = photo.caption
               // commentLabel.text = photo.comment
            }
        }
    }
}
