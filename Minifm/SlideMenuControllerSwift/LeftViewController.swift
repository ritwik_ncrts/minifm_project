//
//  LeftViewController.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 12/3/14.
//

import UIKit

enum LeftMenu: Int {
    case sell = 0
    case shop
    case gallery
    case inbox
    case friends
    case help
    
    
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}
class LeftViewController : UIViewController,UICollectionViewDataSource, UICollectionViewDelegate, LeftMenuProtocol {
    
    func changeViewController(_ menu: LeftMenu) {
        print("Check")
        
        
    }
    
    
    
    
    @IBOutlet weak var lblCrat: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    var menus = ["Home","My Booking","Payment","Account","Notification","Support & Contact us","Logout"]
    
    let reuseIdentifier = "cell" // also enter this string as the cell identifier in the storyboard
    var items = ["Sell", "Shop", "Gallery", "Inbox", "Friends", "Help"]
    
    var itemsPic = ["left-menu-icon-sell", "left-menu-icon-shop","left-menu-icon-gallery","left-menu-icon-inbox", "left-menu-icon-friends", "left-menu-icon-help", ]
    

    var sellViewController: UIViewController!
    var shopViewController: UIViewController!
    var galleryViewController: UIViewController!
    var inboxViewController:UIViewController!
    var friendsViewController:UIViewController!
    var helpsViewController:UIViewController!
    var cartViewController:UIViewController!
    
    var settingViewController:UIViewController!
    
    var imageHeaderView: ImageHeaderView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
       // self.tableView.separatorColor = UIColor(red: 224/255, green: 224/255, blue: 224/255, alpha: 1.0)
      //  self.tableView.tableFooterView = UIView(frame: CGRect.zero)
      //  let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let settingView =  SettingsViewController()
        self.settingViewController = UINavigationController(rootViewController: settingView)
        
        let swiftViewController =  SellViewController()
        self.sellViewController = UINavigationController(rootViewController: swiftViewController)
        
        let shopView = ListViewController()
        self.shopViewController = UINavigationController(rootViewController: shopView)
        
        let galleryView = GalleryViewController()
        self.galleryViewController = UINavigationController(rootViewController: galleryView)

        let inboxView = InboxViewController()
        self.inboxViewController = UINavigationController(rootViewController: inboxView)
        
        let friendsView = FriendsViewController()
        self.friendsViewController = UINavigationController(rootViewController: friendsView)
        
        let helpsView = ListViewController()
        self.helpsViewController = UINavigationController(rootViewController: helpsView)
        
        let cartView = CartViewController()
        self.cartViewController = UINavigationController(rootViewController: cartView)
        
      /*  let rootController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.rootViewController = UINavigationController(rootViewController: rootController)*/
        
        
    //    self.tableView.registerCellClass(BaseTableViewCell.self)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //self.imageHeaderView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 160)  ritwik
        self.view.layoutIfNeeded()
    }
    
/*    func changeViewController(_ menu: LeftMenu) {
        switch menu {
        case .main:
            self.sellViewController()?.changeMainViewController(self.mainViewController, close: true)
        case .booked:
            self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
        case .Payment:
            self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
        case .Account:
            self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
        case .Notification:
            self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
        case .Support:
            self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
        case .Logout:
            let defaults = UserDefaults.standard
            defaults.removeObject(forKey:"user_id")
            defaults.synchronize()
            self.slideMenuController()?.changeMainViewController(self.rootViewController, close: true)
        
        }
    }*/
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! MyCollectionViewCell
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        cell.lblName.text = self.items[indexPath.item]
        cell.imgIcon.image = UIImage(named:self.itemsPic[indexPath.item])
        cell.imgIcon.contentMode = UIViewContentMode.scaleAspectFit
        //cell.backgroundColor = UIColor.clear // make cell more visible in our example project
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        
        if indexPath.item == 0 {
            
            self.slideMenuController()?.changeMainViewController(self.sellViewController, close: true)
        }else if indexPath.item == 1 {
            
            self.slideMenuController()?.changeMainViewController(self.shopViewController, close: true)
        }else if indexPath.item == 2 {
            
            self.slideMenuController()?.changeMainViewController(self.galleryViewController, close: true)
        }else if indexPath.item == 3 {
            
            self.slideMenuController()?.changeMainViewController(self.inboxViewController, close: true)
        }else if indexPath.item == 4 {
            
            self.slideMenuController()?.changeMainViewController(self.friendsViewController, close: true)
        }else if indexPath.item == 5 {
            
            self.slideMenuController()?.changeMainViewController(self.helpsViewController, close: true)
        }
    }

    @IBAction func cartAction(_ sender: Any) {
        
       // let cartView = CartViewController()
        //self.cartViewController = UINavigationController(rootViewController: cartView)
        self.slideMenuController()?.changeMainViewController(self.cartViewController, close: true)
    }
    
    @IBAction func settingsAction(_ sender: Any) {
        
        self.slideMenuController()?.changeMainViewController(self.settingViewController, close: true)
    }
}





/*extension LeftViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let menu = LeftMenu(rawValue: indexPath.row) {
            switch menu {
            case .main, .booked, .Payment ,.Account,.Notification,.Support,.Logout:
                return BaseTableViewCell.height()
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = LeftMenu(rawValue: indexPath.row) {
            self.changeViewController(menu)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tableView == scrollView {
            
        }
    }
}

extension LeftViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let menu = LeftMenu(rawValue: indexPath.row) {
            switch menu {
            case .main, .booked,.Payment,.Account,.Notification,.Support,.Logout:
                let cell = BaseTableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: BaseTableViewCell.identifier)
                cell.setData(menus[indexPath.row])
                return cell
            }
        }
        return UITableViewCell()
    }
}*/
